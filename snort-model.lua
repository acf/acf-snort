local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")

-- Set variables
local packagename = "snort"
local processname = "snort"
local configfile = "/etc/snort/snort.conf"
local alertfile = "/var/log/snort/alert"

-- ################################################################################
-- LOCAL FUNCTIONS

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "Snort Status")
end

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.read_alert()
	local alertpriority = {}
	local liboutput = fs.read_file_as_array(alertfile)
	if (liboutput) then
		for i,line in ipairs(liboutput) do
			--DEBUG
			--if (i == 1) then break end
			local currid = string.match(line, "^.*%[%*%*%]%s*%[(%d+:%d+:%d+)%]")
			if (currid) then
				local priority = string.match(liboutput[i+1],"^.*%[.*lassification:%s*.*%]%s*%[(.*)%]") or "Priority: Unknown"
				local classification = string.match(liboutput[i+1],"^.*%[.*lassification:%s*(.*)%]%s*%[") or "Classification: Unknown"
				if (alertpriority[priority] == nil) then
					alertpriority[priority] = {}
				end
				if (alertpriority[priority][classification] == nil) then
					alertpriority[priority][classification] = {}
				end
				if (alertpriority[priority][classification][currid] == nil) then
					alertpriority[priority][classification][currid] = { value={}, url={}, count=0 }

					local rowvalue = line
					local j = 0
					while rowvalue and rowvalue ~= "" do
						if string.match(rowvalue, "%[Xref.*") ~= nil then
							for v in string.gmatch(rowvalue, "%[Xref%s+%=%>%s+(.-)%]") do
								table.insert(alertpriority[priority][classification][currid]["url"],v)
							end
						elseif string.match(rowvalue, "%[Classification.*") == nil then
							table.insert(alertpriority[priority][classification][currid].value,rowvalue)
						end
						j=j+1
						rowvalue = liboutput[i+j]
					end
				end
				alertpriority[priority][classification][currid].count = alertpriority[priority][classification][currid].count + 1
			end
		end
	end
	--Start sorting priority-table
	local sorted_table = {}
	for name,value in pairs(alertpriority) do
		table.insert(sorted_table, {name=name, value=value})
	end
	table.sort(sorted_table, function(a,b) return (a.name < b.name) end)

	return cfe({ type="structure", value=sorted_table, label="Snort Alerts" })
end

function mymodule.get_filedetails()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.update_filedetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

return mymodule
