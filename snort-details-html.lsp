<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% viewlibrary.dispatch_component("status") %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% local header_level2 = htmlviewfunctions.incrementheader(header_level) %>
<% local header_level3 = htmlviewfunctions.incrementheader(header_level2) %>
<%
if #data.value == 0 then
	io.write("<p>No alerts found</p>")
else
	for i,priority in ipairs(data.value) do %>
		<% htmlviewfunctions.displaysectionstart(cfe({label=priority.name}), page_info, header_level2) %>
		<% for cls in pairs(priority.value) do %>
			<% htmlviewfunctions.displaysectionstart(cfe({label=cls}), page_info, header_level3) %>
			<% for id,alert in pairs(priority.value[cls]) do %>
				<p><b><%= html.html_escape(alert.value[1]) %></b><br/>
				<% for j=2, #alert.value do %>
					<%= html.html_escape(alert.value[j]) %><br/>
				<% end %>
				<i>(This alarm is repeated <b><%= html.html_escape(alert.count) %></b> times)</i>
				<% if (#alert.url ~= 0) then %>
					( URL's:
					<% for k,url in ipairs(alert.url) do %>
						<a href="<%= html.html_escape(url) %>" target="_new"><img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/categories/applications-internet.png" alt="<%= html.html_escape(url) %>"></a>
					<% end %>
				)
				<% end %>
				</p>
			<% end %>
			<% htmlviewfunctions.displaysectionend(header_level3) %>
		<% end %>
		<% htmlviewfunctions.displaysectionend(header_level2) %>
	<% end %>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
